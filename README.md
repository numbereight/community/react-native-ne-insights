# NumberEight Insights for React Native

## Getting started

You will first need to [**generate a developer key**](https://portal.numbereight.me/keys) to authenticate with.

To install the plugin, run

```
$ yarn add https://gitlab.com/numbereight/react-native-ne-insights
OR
$ npm install --save git+https://gitlab.com/numbereight/react-native-ne-insights
```

### Running the provided example project

If you don't yet have a React Native project, you can use the provided example as follows (you must have yarn installed):

1. Start the server:
```
cd example
# Install dependencies
yarn
# Put developer key in an environment variable
# (change <insert key> to your developer key)
echo 'NESDK_KEY=<insert key>' >> .env
# Start server
yarn start
```

2. Run Android:
```
# Allow communication on port 8081
adb reverse tcp:8081 tcp:8081
# Build and run app
yarn android
```

3. Run iOS:
```
# Build and run app
yarn ios
```

#### Platform-specific instructions

If required, refer to the official documentation:

* [Android](https://docs.numbereight.me/#/android-insights)
* [iOS](https://docs.numbereight.me/#/ios-insights)

#### Native repositories

You must make sure to add the NumberEight repository to your Android and iOS projects:

##### Android (build.gradle)

```
allprojects {
    repositories {
        maven {
            url('http://repo.numbereight.me/artifactory/gradle-release-local')
        }
    }
}
```


##### iOS (Podfile)

```
source 'https://gitlab.com/numbereight/cocoapods'
source 'https://github.com/CocoaPods/Specs.git'
```

#### Permissions

While no permissions are required, for optimal performance and to avoid app store restrictions the recommended permission setups:

*  [AndroidManifest.xml for Android](https://docs.numbereight.me/#/android-getting-started?id=_1-configure-permissions-for-your-project-in-androidmanifestxml)
*  [Info.plist for iOS](https://portal.eu.numbereight.me/documentation/#/ios-getting-started?id=_1-add-the-following-keys-to-the-projects-infoplist)

##### Location

In addition to adding the required settings to AndroidManifest.xml and Info.plist (see above links), some permissions such as location require a prompt to the user. NumberEight recommends using [react-native-permissions](https://github.com/react-native-community/react-native-permissions) for this task:

```
$ yarn add react-native-permissions
OR
$ npm install --save react-native-permissions
```

```
import { Platform } from 'react-native';
import { PERMISSIONS, request } from 'react-native-permissions';

request(
  Platform.select({
    android: PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION,
    ios: PERMISSIONS.IOS.LOCATION_ALWAYS,
  }),
);
```

## Usage

It's recommended that you put your API key in a [`.env` file](https://github.com/zetachang/react-native-dotenv).

```javascript
import Insights from 'react-native-ne-insights';
import { NESDK_KEY } from 'react-native-dotenv';

export default class App extends Component<{}> {
  componentDidMount() {
  
    // Start recording context to NumberEight's server
    Insights.startRecording(NESDK_KEY);
    
    // Add markers to record context for in-app events
    Insights.addMarker("purchase_made");
    
  }
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Insights is working!</Text>
      </View>
    );
  }
}
```

### Maintainers

* [Chris Watts](chris@numbereight.me)