package me.numbereight.react;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import me.numbereight.insights.Insights;
import me.numbereight.sdk.NumberEight;

public class InsightsModule extends ReactContextBaseJavaModule {
    private final ReactApplicationContext reactContext;

    public InsightsModule(ReactApplicationContext reactContext) {
        super(reactContext);
        this.reactContext = reactContext;
    }

    @Override
    public String getName() {
        return "Insights";
    }

    @ReactMethod
    public void startRecording(String apiKey) {
        NumberEight.APIToken token = NumberEight.start(apiKey, reactContext);
        if (token != null) {
            Insights.startRecording(token);
        }
    }

    @ReactMethod
    public void endRecording() {
        Insights.endRecording();
    }

    @ReactMethod
    public void addMarker(String name) {
        Insights.addMarker(name);
    }
}
