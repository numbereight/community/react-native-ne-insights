import { NativeModules } from 'react-native';

const { Insights } = NativeModules;

export default Insights;
