#import "RCTInsights.h"
@import Insights;

@implementation RCTInsights

RCT_EXPORT_MODULE()

RCT_EXPORT_METHOD(startRecording:(NSString*)apiKey)
{
    NEXAPIToken *token = [NEXNumberEight startWithApiKey:apiKey
                                           launchOptions:nil
                                              completion:nil];
    if (token != nil) {
        [NEXInsights startRecordingWithApiToken:token
                                         config:NEXRecordingConfig.defaultConfig];
    }
}

RCT_EXPORT_METHOD(endRecording)
{
    [NEXInsights endRecording];
}

RCT_EXPORT_METHOD(addMarker:(NSString*)name)
{
    [NEXInsights addMarker:name];
}

@end
