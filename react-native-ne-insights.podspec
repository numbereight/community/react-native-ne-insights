require "json"

package = JSON.parse(File.read(File.join(__dir__, "package.json")))

Pod::Spec.new do |s|
  s.name         = "react-native-ne-insights"
  s.version      = package["version"]
  s.summary      = package["description"]
  s.description  = <<-DESC
                  react-native-ne-insights
                   DESC
  s.homepage     = "https://gitlab.com/numbereight/react-native-ne-insights"
  s.license      = { :type => "BSD", :file => "LICENSE" }
  s.authors      = { "NumberEight Technologies Ltd" => "support@numbereight.me" }
  s.platforms    = { :ios => "9.0" }
  s.source       = { :git => "https://gitlab.com/numbereight/react-native-ne-insights.git", :tag => "#{s.version}" }

  s.source_files = "ios/**/*.{h,m,swift}"
  s.requires_arc = true
  s.swift_version = '5'

  s.dependency "React"
  s.dependency "NumberEight/Insights"
end

